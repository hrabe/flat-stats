import json
import requests
from datetime import datetime

class BezrealitkyParser:
    
    def parse(self, url, responses):
        data = requests.get(url)
        binary = data.content
        json_data = json.loads(binary)
        count = len(json_data)
        print('Found {estates_count} estates.'.format(estates_count=count))
        for i in range(0,count):
            hash_id = json_data[i]['id']
            locality = json_data[i]['uri']
            price = json_data[i]['advertEstateOffer'][0]['price']
            if price == 1:
                continue
            area = int(json_data[i]['advertEstateOffer'][0]['surface'])
            str_time = json_data[i]['timeOrder']['date']
            datetime_object = datetime.strptime(str_time, '%Y-%m-%d %H:%M:%S.%f')
            ltime = datetime_object.strftime("%Y-%m-%dT%H:%M:%S") + ".%03d" % (datetime_object.microsecond / 1000) + "Z"

            price_norm = (int)(price/(area + 0.1))
            data = {
                'hash_id' : hash_id,
                'locality' : locality,
                'price' : price,
                'area' : area,
                'price_norm' : price_norm,
                'timestamp': ltime
            }
            responses.append(data)
    pass