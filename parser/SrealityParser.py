import json
import requests
from datetime import datetime

class SrealityParser:
    
    def parse(self, url, responses):
        data = requests.get(url)
        binary = data.content
        json_data = json.loads(binary)
        count = len(json_data['_embedded']['estates'])
        print('Found {estates_count} estates.'.format(estates_count=count))
        for i in range(0,count):
            hash_id = json_data['_embedded']['estates'][i]['hash_id']
            locality = json_data['_embedded']['estates'][i]['locality']
            price = json_data['_embedded']['estates'][i]['price_czk']['value_raw']
            if price == 1:
                continue
            area = int(str(json_data['_embedded']['estates'][i]['name']).split()[-2])
            
            now = datetime.utcnow()
            ltime = now.strftime("%Y-%m-%dT%H:%M:%S") + ".%03d" % (now.microsecond / 1000) + "Z"

            price_norm = (int)(price/(area + 0.1))
            data = {
                'hash_id' : hash_id,
                'locality' : locality,
                'price' : price,
                'area' : area,
                'price_norm' : price_norm,
                'timestamp': ltime
            }
            responses.append(data)
    pass