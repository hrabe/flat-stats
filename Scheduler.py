import time, schedule
import signal
from service.FlatService import FlatService
import configparser
import os
from twisted.internet import task
from twisted.internet import reactor

from scrapy.crawler import CrawlerProcess, CrawlerRunner
from scrapy.utils.project import get_project_settings, init_env, import_module
from scrapy_crawler.reality_crawlers import settings as custom_settings
from scrapy.settings import Settings
from apscheduler.schedulers.twisted import TwistedScheduler
from scrapy_crawler.ScrapyApi import ScrapyApi


from scrapy_crawler.reality_crawlers.spiders.RealitySpider import RealitySpider

class ServiceExit(Exception):
    """
    Custom exception which is used to trigger the clean exit
    of all running threads and the main program.
    """
    pass
def rest_api_function():
    echo_time()
    flat_service = FlatService(elastic_hosts)
    flat_service.post_data_to_elastic()
    pass

def crawler_function():
    echo_time()
    scraper = ScrapyApi(elastic_hosts, headless_chrome_url)
    scraper.run_spiders()
    pass

def echo_time():
    t = (int)(time.time())
    print(time.strftime("%b %d %Y %H:%M:%S", time.gmtime(t)))
    pass

def service_shutdown(signum, frame):
    print('Caught signal %d' % signum)
    raise ServiceExit

def main():
 
    # Register the signal handlers
    signal.signal(signal.SIGTERM, service_shutdown)
    signal.signal(signal.SIGINT, service_shutdown)
    
    config = configparser.ConfigParser()
    config.read('config.ini')
    
    global elastic_hosts, headless_chrome_url
    elastic_hosts = os.environ.get('ELASTICSEARCH_URL', config['DEFAULT']['ELASTIC_HOSTS'])
    headless_chrome_url = os.environ.get('HEADLESS_CHROME_URL', config['DEFAULT']['HEADLESS_CHROME_URL'])

    try:
        scraper = task.LoopingCall(crawler_function)
        scraper.start(10*60, now=False) # call in seconds

        api = task.LoopingCall(rest_api_function)
        api.start(5*60, now=False) # call in seconds
        reactor.run()
    except ServiceExit:
        scraper.stop()
        api.stop()

if __name__ == '__main__':
    main()