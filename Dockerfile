
FROM python:3.7.3-stretch
# important X server virtualization => xvfb

RUN apt-get update && apt-get -y install \
  python3-lxml \
  build-essential \
  libssl-dev \
  libffi-dev \
  python3-dev \
  libxml2-dev \
  libxslt1-dev \
  udev \
  gcc \
  && rm -rf /var/lib/apt/lists/*

# Installation Directory 
ENV INSTALL_PATH /app

 # Python will not buffer output to stdout 
ENV PYTHONUNBUFFERED 1

# Set the working directory to /app 
WORKDIR $INSTALL_PATH

# Copy the current directory contents into the container at /app 
COPY . $INSTALL_PATH

# Install requirements for app, --no-cache-dir to not cache
# RUN pip3 install -r requirements.txt
RUN pip install --cache-dir=.pip -r requirements.txt

# Expose 443
EXPOSE 443

# Run repeat.py when the container launches with logs 
CMD ["/bin/sh", "-c", "python Scheduler.py"]