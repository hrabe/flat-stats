from datetime import datetime
from elasticsearch import Elasticsearch

class ElasticsearchConnector:

    def __init__(self, hosts):
        self.es = Elasticsearch([hosts],maxsize=25)
    """
    doc_type = tweet
    index_name = test_index 
    """
    def exists_in_elastic(self, hash_id, index_name, doc_type):
        res = self.es.exists(index=str(index_name), doc_type=str(doc_type), id=hash_id)
        return res
    def send_doc_to_elastic(self, hash_id, index_name, doc_type, doc):
        self.es.index(index=str(index_name), doc_type=str(doc_type), id=hash_id, body=doc)
        self.refresh_elastic_index(index_name)
        pass
    def refresh_elastic_index(self, index_name):
        self.es.indices.refresh(index=index_name)
        pass

