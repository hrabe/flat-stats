from connector.ElasticsearchConnector import ElasticsearchConnector
from builder.RequestsBuilder import RequestsBuilder

class FlatService:
    
    def __init__(self, elastic_hosts):
        self.elastic_connector = ElasticsearchConnector(elastic_hosts)
        self.requestBuilder = RequestsBuilder()

    def post_data_to_elastic(self):
        docs = self.requestBuilder.get_data()
        for doc in docs:
            if not self.elastic_connector.exists_in_elastic(doc['hash_id'], 'api-index', 'reality'):
                self.elastic_connector.send_doc_to_elastic(doc['hash_id'], 'api-index', 'reality', doc)
        pass