#import grequests
import requests
import json
from datetime import datetime
from builder.SrealityBuilder import SrealityBuilder
from builder.BezrealitkyBuilder import BezrealitkyBuilder
from parser.BezrealitkyParser import BezrealitkyParser
from parser.SrealityParser import SrealityParser

class RequestsBuilder:
    def __init__(self):
        self.urls = {
            'sreality': self.get_sreality_query(),
            'bezrealitky': self.get_bezrealitky_query()
        }

    def get_sreality_query(self):
        srealityBuilder = SrealityBuilder()
        srealityBuilder.set_estate_type('apartments')
        srealityBuilder.set_disposition('all-disposition')
        srealityBuilder.set_offer_type('sale')
        srealityBuilder.set_locality_region('plzensky')
        srealityBuilder.set_locality_district('plzen-mesto')
        srealityBuilder.set_estate_age('past_week')
        srealityBuilder.set_pages('60')
        return srealityBuilder.getFullQuery()
    
    def get_bezrealitky_query(self):
        bezrealitkyBuilder = BezrealitkyBuilder()
        bezrealitkyBuilder.set_estate_type('apartments')
        bezrealitkyBuilder.set_disposition('all-disposition')
        bezrealitkyBuilder.set_offer_type('sale')
        bezrealitkyBuilder.set_locality_boundary('plzen')
        return bezrealitkyBuilder.getFullQuery()

    def get_data(self):
        responses = []
        for origin in self.urls:
            #need to parse request
            print(origin)
            if origin == 'sreality':
                srealityParser = SrealityParser()
                srealityParser.parse(self.urls[origin], responses)
            elif origin == 'bezrealitky':
                bezrealitkyParser = BezrealitkyParser()
                bezrealitkyParser.parse(self.urls[origin], responses)
                
        return responses