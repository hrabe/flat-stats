""" This is sreality builder, it maps their api
    and creates full api request"""
class SrealityBuilder:
    # Class Attribute
    baseApiUrl = "https://www.sreality.cz/api/cs/v2/estates"

    def __init__(self, estateType='apartments', disposition='all-disposition', offerType='sale', localityRegion='plzensky', localityDistrict='plzen-mesto'):
        self.set_estate_type(estateType)
        self.set_disposition(disposition)
        self.set_offer_type(offerType)
        self.set_locality_region(localityRegion)
        self.set_locality_district(localityDistrict)

    def set_estate_type(self, estateType):
        choices = {'houses': '2', 'apartments': '1'}
        # 1 stands for default value houses
        self.estateType = 'category_main_cb=' + choices.get(estateType, '1')
        pass
    
    def set_disposition(self, disposition):
        choices = {'all-disposition': '2|3|4|5|6','1+kk': '2', '1+1': '3', '2+kk':'4', '2+1':'5', '3+kk':'6'}
        # 6 stands for default value 3+kk
        self.disposition = 'category_sub_cb=' + choices.get(disposition, '6')
        pass
    
    def set_offer_type(self, offerType):
        choices = {'sale': '1', 'lease': '2'}
        # 1 stands for default value sale
        self.offerType = 'category_type_cb=' + choices.get(offerType, '1')
        pass
    
    def set_locality_region(self, localityRegion):
        choices = {'plzensky': '2', 'prazsky': '10'}
        # 1 stands for default value sale
        self.localityRegion = 'locality_region_id=' + choices.get(localityRegion, '2')
        pass

    def set_locality_district(self, localityDistrict):
        choices = {'plzen-mesto': '12'}
        # 1 stands for default value sale
        self.localityDistrict = 'locality_district_id=' + choices.get(localityDistrict, '12')
        pass

    def set_pages(self, perPage):
        choices = {'20': '20', '40': '40','60': '60',}
        self.perPage = 'per_page=' + choices.get(perPage, '60')
    
    def set_estate_age(self, estateAge):
        choices = {'past_day': '2', 'past_week': '8'}
        self.estateAge = 'estate_age=' + choices.get(estateAge, '8')

    def getFullQuery(self):
        return ('{baseUrl}?{estateType}&{disposition}&{offerType}'
        '&{localityRegion}&{localityDistrict}&{perPage}&{estateAge}').format(
        baseUrl = self.baseApiUrl, estateType=self.estateType, 
        disposition=self.disposition, offerType=self.offerType, 
        localityRegion=self.localityRegion, localityDistrict=self.localityDistrict,
        perPage=self.perPage, estateAge=self.estateAge)
