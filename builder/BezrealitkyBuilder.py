""" This is bezrealitky builder, it maps their api
    and creates full api request"""
class BezrealitkyBuilder:
    # Class Attribute
    baseApiUrl = "https://www.bezrealitky.cz/api/record/markers"

    def __init__(self, estateType='apartments', disposition='all-disposition', offerType='sale', localityBoundary='plzen'):
        self.set_estate_type(estateType)
        self.set_disposition(disposition)
        self.set_offer_type(offerType)
        self.set_locality_boundary(localityBoundary)

    def set_estate_type(self, estateType):
        choices = {'houses': 'dum', 'apartments': 'byt'}
        self.estateType = 'estateType=' + choices.get(estateType, 'byt')
        pass
    
    def set_disposition(self, disposition):
        choices = {'all-disposition': '1-kk,1-1,2-kk,2-1,3-kk','1+kk': '1-kk', '1+1': '1-1', '2+kk':'2-kk', '2+1':'2-1', '3+kk':'3-kk'}
        self.disposition = 'disposition=' + choices.get(disposition, '1-kk,1-1,2-kk,2-1,3-kk')
        pass
    
    def set_offer_type(self, offerType):
        choices = {'sale': 'prodej', 'lease': 'pronajem'}
        # 1 stands for default value sale
        self.offerType = 'offerType=' + choices.get(offerType, 'prodej')
        pass
    
    def set_locality_boundary(self, localityBoundary):
        choices = {'plzen': '[[{"lat":49.800734732111,"lng":13.351515627608023},{"lat":49.805634522229,"lng":13.402224296452005},{"lat":49.778898559896,"lng":13.414462289905941},{"lat":49.787257092501,"lng":13.430317389529023},{"lat":49.784920001782,"lng":13.434908341671985},{"lat":49.777734957264,"lng":13.428155415958031},{"lat":49.774631409937,"lng":13.436393950071988},{"lat":49.7751217746,"lng":13.471800776035025},{"lat":49.762834066389,"lng":13.46980531984002},{"lat":49.761996567331,"lng":13.475825867957042},{"lat":49.739251795653,"lng":13.467034315204046},{"lat":49.735033778845,"lng":13.450543818200003},{"lat":49.728259295577,"lng":13.453772276507038},{"lat":49.707708338901,"lng":13.445057695398987},{"lat":49.710602713164,"lng":13.437546506640047},{"lat":49.7030151382,"lng":13.429023154409037},{"lat":49.684409211677,"lng":13.447959363947007},{"lat":49.677600522153,"lng":13.426798371851987},{"lat":49.681312718165,"lng":13.401605803215034},{"lat":49.688415407625,"lng":13.403596916662991},{"lat":49.689073277777,"lng":13.376578237844},{"lat":49.680854727456,"lng":13.365165037047973},{"lat":49.684544722523,"lng":13.324190310730955},{"lat":49.679179382127,"lng":13.315779771145003},{"lat":49.68445940259,"lng":13.302360265557013},{"lat":49.695552326131,"lng":13.30476098135},{"lat":49.716856258715,"lng":13.294482113783943},{"lat":49.724691012417,"lng":13.305907950742949},{"lat":49.733835006366,"lng":13.297600298191014},{"lat":49.735717210151,"lng":13.308076634563008},{"lat":49.737678812173,"lng":13.285457550134993},{"lat":49.765375638837,"lng":13.291687564667996},{"lat":49.773971621837,"lng":13.267989006548987},{"lat":49.795042218415,"lng":13.288128817010033},{"lat":49.777962124079,"lng":13.325509450635991},{"lat":49.776640327281,"lng":13.351550588982946},{"lat":49.786930998016,"lng":13.363199031289014},{"lat":49.800734732111,"lng":13.351515627608023},{"lat":49.800734732111,"lng":13.351515627608023}]]'}
        # 1 stands for default value sale
        self.localityBoundary = 'boundary=' + choices.get(localityBoundary, 'plzen')
        pass
    
    def getFullQuery(self):
        return ('{baseUrl}?{estateType}&{disposition}&{offerType}'
        '&{localityBoundary}').format(
        baseUrl = self.baseApiUrl, estateType=self.estateType, 
        disposition=self.disposition, offerType=self.offerType, 
        localityBoundary=self.localityBoundary)
