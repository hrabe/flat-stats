# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class SpiderTestItem(scrapy.Item):
    hash_id = scrapy.Field()
    price = scrapy.Field()
    disposition = scrapy.Field()
    timestamp = scrapy.Field()
    area = scrapy.Field()
    price_norm = scrapy.Field()
    locality = scrapy.Field()
    url = scrapy.Field()
    pass
