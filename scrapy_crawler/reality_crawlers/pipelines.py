import sys
import os

parentDir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
# newPath = os.path.join(parentDir, 'connector')   # Get the directory for StringFunctions
sys.path.append(parentDir)  

from connector.ElasticsearchConnector import ElasticsearchConnector
# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

class RealitySpiderPipeline(object):
    collection_name = 'scrapy_items'

    def __init__(self, elastic_hosts):
        self.elastic_hosts = elastic_hosts

    @classmethod
    def from_crawler(cls, crawler):
        print('fromCrawler')
        print(crawler.settings.get('ELASTIC_HOSTS')) 
        return cls(
            elastic_hosts=crawler.settings.get('ELASTIC_HOSTS')
        )

    def open_spider(self, spider):
        print('openSp')
        self.elastic_connector = ElasticsearchConnector(self.elastic_hosts)

    def close_spider(self, spider):
        pass

    def process_item(self, item, spider):
        print('pipelineElastic')
        if not self.elastic_connector.exists_in_elastic(item['hash_id'], 'crawler-index', 'reality'):
                self.elastic_connector.send_doc_to_elastic(item['hash_id'], 'crawler-index', 'reality', item)
        return item
