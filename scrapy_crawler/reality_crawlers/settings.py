# -*- coding: utf-8 -*-
#
# Scrapy settings for spider_test project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://doc.scrapy.org/en/latest/topics/settings.html
#     https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://doc.scrapy.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'RealityBot'

SPIDER_MODULES = ['scrapy_crawler.reality_crawlers.spiders']
NEWSPIDER_MODULE = 'scrapy_crawler.reality_crawlers.spiders'

ELASTIC_HOSTS = 'localhost:9200'

DEPTH_LIMIT = 1

# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.34 Safari/534.24'

# Obey robots.txt rules. Don't get into troubles
ROBOTSTXT_OBEY = True

# Configure maximum concurrent requests performed by Scrapy (default: 16)
# A good starting point is 100, but the best way to find out is by doing some 
# trials and identifying at what concurrency your Scrapy process gets CPU bounded.
# For optimum performance, you should pick a concurrency where CPU usage is at 80-90%.
CONCURRENT_REQUESTS = 100

# The DNS queue will be processed faster speeding up establishing of connection
# and crawling overall. To increase maximum thread pool size use:
REACTOR_THREADPOOL_MAXSIZE = 20

# Log level info for important messages, use DEBUG in emergency case.
# INFO level is chosen due performance impact compared to DEBUG level mode.
LOG_LEVEL = 'INFO'

# Disable cookies unless you really need. Cookies are often not needed when
# doing broad crawls (search engine crawlers ignore them), and they improve
# performance by saving some CPU cycles and reducing the memory footprint of
# your Scrapy crawler. To disable cookies use:
COOKIES_ENABLED = False

# Retrying failed HTTP requests can slow down the crawls substantially, specially
# when sites causes are very slow (or fail) to respond, thus causing a timeout
# error which gets retried many times, unnecessarily, preventing crawler capacity
# to be reused for other domains. To disable retries use:
RETRY_ENABLED = False

# Unless you are crawling from a very slow connection (which shouldn’t be the case
# for broad crawls) reduce the download timeout so that stuck requests are
# discarded quickly and free up capacity to process the next ones. To reduce
# the download timeout use:
DOWNLOAD_TIMEOUT = 5

# Consider disabling redirects, unless you are interested in following them.
# When doing broad crawls it’s common to save redirects and resolve them when
# revisiting the site at a later crawl. This also help to keep the number
# of request constant per crawl batch, otherwise redirect loops may cause the
# crawler to dedicate too many resources on any specific domain. To disable 
# redirects use:
REDIRECT_ENABLED = False

# Configure a delay for requests for the same website (default: 0)
# See https://doc.scrapy.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
DOWNLOAD_DELAY = 10

# Enable AJAX generated web pages
AJAXCRAWL_ENABLED = True

DEPTH_PRIORITY = 1
SCHEDULER_DISK_QUEUE = 'scrapy.squeues.PickleFifoDiskQueue'
SCHEDULER_MEMORY_QUEUE = 'scrapy.squeues.FifoMemoryQueue'

# The download delay setting will honor only one of:
#CONCURRENT_REQUESTS_PER_DOMAIN = 16
#CONCURRENT_REQUESTS_PER_IP = 16

# Disable Telnet Console (enabled by default)
#TELNETCONSOLE_ENABLED = False

# Override the default request headers:
#DEFAULT_REQUEST_HEADERS = {
#   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#   'Accept-Language': 'en',
#}

# Enable or disable spider middlewares
# See https://doc.scrapy.org/en/latest/topics/spider-middleware.html
#SPIDER_MIDDLEWARES = {
#    'spider_test.middlewares.SpiderTestSpiderMiddleware': 543,
#}

# Enable or disable downloader middlewares
# See https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
DOWNLOADER_MIDDLEWARES = {
    'scrapy_crawler.reality_crawlers.middlewares.SpiderTestDownloaderMiddleware': 543,
     # 'scrapy.contrib.downloadermiddleware.httpcompression.HttpCompressionMiddleware': None
}

# Enable or disable extensions
# See https://doc.scrapy.org/en/latest/topics/extensions.html
#EXTENSIONS = {
#    'scrapy.extensions.telnet.TelnetConsole': None,
#}

# Configure item pipelines
# See https://doc.scrapy.org/en/latest/topics/item-pipeline.html
ITEM_PIPELINES = {
     'scrapy_crawler.reality_crawlers.pipelines.RealitySpiderPipeline': 300,
 }

# Enable and configure the AutoThrottle extension (disabled by default)
# See https://doc.scrapy.org/en/latest/topics/autphantomjsphantomjsothrottle.html
#AUTOTHROTTLE_ENABLED = True
# The initial download delay
#AUTOTHROTTLE_START_DELAY = 5
# The maximum download delay to be set in case of high latencies
#AUTOTHROTTLE_MAX_DELAY = 60
# The average number of requests Scrapy should be sending in parallel to
# each remote server
#AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0
# Enable showing throttling stats for every response received:
#AUTOTHROTTLE_DEBUG = False

# Enable and configure HTTP caching (disabled by default)
# See https://doc.scrapy.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
#HTTPCACHE_ENABLED = True
#HTTPCACHE_EXPIRATION_SECS = 0
#HTTPCACHE_DIR = 'httpcache'
#HTTPCACHE_IGNORE_HTTP_CODES = []
#HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'





