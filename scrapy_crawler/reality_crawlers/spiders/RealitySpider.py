# -*- coding: utf-8 -*-
import scrapy
import logging
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from bs4 import BeautifulSoup
import re
from urllib.parse import urlparse
import hashlib
from datetime import datetime
import collections

logger = logging.getLogger('scrapy_logger')


class RealitySpider(CrawlSpider):
    name = 'RealitySpider'
    allowed_domains = ['reality.idnes.cz', 'www.reality.cz', 'www.sreality.cz']

    start_urls = [
        'https://reality.idnes.cz/s/prodej/byty/plzen-mesto/', 
        'https://www.reality.cz/prodej/byty/mesto-Plzen',
        'https://www.sreality.cz/hledani/prodej/byty/plzen'
        ]

    rules = (
        # Rule(LinkExtractor(allow=r'^(?=.*(prodej|sale))(?=.*(byty|apartments)).*$', deny=r'(plzen|Plzen)'), follow=True),
        Rule(LinkExtractor(allow=r'^(?=.*(prodej|sale))(?=.*(byty|apartments))(?=.*\b(plzen|Plzen)\b).*$', deny=r'(plzensky|Plzensky)'), callback='parse_page', follow=True),
    )

    def lowest_common_ancestor(self, tags):
        if tags is None or len(tags) <= 1:
            return None
        parentTags = [tag.parent for tag in tags if tag is not None]
        if all(v == parentTags[0] for v in parentTags):
            return tags
        else:
            return self.lowest_common_ancestor(parentTags)


    def parse_item(self, rootTag, url):
        areaTag = rootTag.find(text=re.compile(r"\+.+\d{1,3}\s*m²")).parent
        if areaTag is not None:
            priceRaw = rootTag.find(text=re.compile(r"\d{1}[\s,\.]*\d{3}[\s,\.]*\d{3}\s*Kč"))
            locality = rootTag.find(text=re.compile(r"\bPlzeň\b"))
            if priceRaw is not None and locality is not None and rootTag.text is not None:
                logger.info(priceRaw)
                priceRaw = priceRaw.strip()
                price = ''.join(ch for ch in priceRaw if ch.isdigit())
                logger.info(priceRaw)
                logger.info(price)
                disposition = re.search(r'^.*(\d+\+(kk|1)).*$', rootTag.text, flags=re.IGNORECASE|re.MULTILINE).group(1)
                area = re.search(r'^.*\s+([\d]+)\s+m².*$', rootTag.text, flags=re.IGNORECASE|re.MULTILINE).group(1)
                
                if price == 0 or area == 0:
                    return None
                else:
                    now = datetime.utcnow()
                    ltime = now.strftime("%Y-%m-%dT%H:%M:%S") + ".%03d" % (now.microsecond / 1000) + "Z"
                    item = {}
                    item['price'] = int(price)
                    item['locallity'] = locality.string.strip()
                    item['area'] = int(area)
                    item['disposition'] = disposition
                    item['hash_id'] = hashlib.md5((price + locality + area + disposition).encode()).hexdigest()
                    item['timestamp'] = ltime
                    item['price_norm'] = (int)(int(price))/(int(area))
                    item['url'] = url
                    return item
        return None

    def parse_page(self, response):
        soup = BeautifulSoup(response.text, 'lxml')

        logger.info('-----------------------------------')
        item = {}
        logger.info('Parse function called on %s', response.url)
        areaTags = soup.find_all(text=re.compile(r"\+.+\d{1,3}\s*m²"))

        if areaTags is not None and len(areaTags) == 1:
            item = self.parse_item(soup, response.url)
        else:
            areaTags = [areaTag.parent for areaTag in areaTags]

        commonAncestors = self.lowest_common_ancestor(areaTags)
        for commonAncestor in commonAncestors:
            item = self.parse_item(commonAncestor, response.url)
            yield item
        
