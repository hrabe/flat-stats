# -*- coding: utf-8 -*-
import scrapy
import logging
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import SitemapSpider, Rule
from bs4 import BeautifulSoup
import re
from urllib.parse import urlparse
import hashlib
from datetime import datetime


logger = logging.getLogger('customlogger')


class RealitySitemapSpider(SitemapSpider):
    name = 'RealitySitemapSpider'
    
    allowed_domains = ['reality.idnes.cz', 'www.reality.cz', 'www.sreality.cz']

    sitemap_urls = [
        'https://reality.idnes.cz/sitemap.xml', 
        'https://www.reality.cz/api/sitemap.xml',
        'https://www.sreality.cz/sitemap.xml'
        ]

    def parse(self, response):
        print(response.url)
        if re.match(r".*(Plzen|Pilsen).*", response.url, re.IGNORECASE) and re.match(r".*(Byty|Apartments|Flats).*", response.url, re.IGNORECASE) and re.match(r".*(Prodej|Sale).*", response.url, re.IGNORECASE):
            print('match!')
            logger.info('-----------------------------------')
            item = {}
            logger.info('Parse function called on %s', response.url)
            soup = BeautifulSoup(response.text, 'lxml')
            for tag in soup.find_all(text=re.compile(r"\+.+\d{1,3}\s*m²")):
                if tag.parent is not None and tag.parent.name != 'script':
                    area_tag=tag.parent
                    # depth=len(list(tag.parents))
                    if area_tag is not None:
                        price_raw = area_tag.parent.find(text=re.compile(r"\d{1}[\s,\.]\d{3}[\s,\.]\d{3}\s*Kč"))
                        locality = area_tag.parent.find(text=re.compile(r"\bPlzeň\b"))
                        if price_raw is not None and locality is not None:
                            price_raw = price_raw.strip()
                            price_raw = price_raw.replace(' ','')
                            price = re.findall('\d+',price_raw)[0]
                            disposition = re.search('^.*(\d+\+(kk|1)).*$', area_tag.text, flags=re.IGNORECASE|re.MULTILINE).group(1)
                            area = re.search('^.*\s+([\d]+)\s+m².*$', area_tag.text, flags=re.IGNORECASE|re.MULTILINE).group(1)
                            now = datetime.utcnow()
                            ltime = now.strftime("%Y-%m-%dT%H:%M:%S") + ".%03d" % (now.microsecond / 1000) + "Z"
                            
                            item['price'] = int(price)
                            item['locallity'] = locality.string.strip()
                            item['area'] = int(area)
                            item['disposition'] = disposition
                            item['hash_id'] = hashlib.md5((price + locality + area + disposition).encode()).hexdigest()
                            item['timestamp'] = ltime
                            item['price_norm'] = (int)(int(price))/(int(area))
                            yield item