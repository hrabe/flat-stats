from scrapy_crawler.reality_crawlers.spiders.RealitySpider import RealitySpider
from scrapy.crawler import CrawlerRunner
from scrapy.utils.project import get_project_settings
from scrapy.utils.log import configure_logging
import os
import logging
from twisted.internet import reactor

class ScrapyApi:
    logger = logging.getLogger('scrapy_logger')
    
    def __init__(self, elastic_hosts, headless_chrome_url):
        settings_file_path = 'scrapy_crawler.reality_crawlers.settings' # The path seen from root, ie. from main.py
        os.environ.setdefault('SCRAPY_SETTINGS_MODULE', settings_file_path)
        
        settings = get_project_settings()

        settings['ELASTIC_HOSTS'] = elastic_hosts
        settings['HEADLESS_CHROME_URL'] = headless_chrome_url
        settings['DEPTH_LIMIT'] = 2

        configure_logging(settings)
        configure_logging(install_root_handler=False)
        logging.basicConfig(
            filename='log.txt',
            format='%(levelname)s: %(message)s',
            level=logging.INFO
        )
        self.runner = CrawlerRunner(settings)
        self.spider = RealitySpider # The spider you want to crawl

    def run_spiders(self):
        self.runner.crawl(self.spider)
        pass