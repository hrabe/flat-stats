Flat statistics
======================

Overview
----------------------

Flat statistics are originally meant to record flat price per m<sup>2</sup> in particular place over time period.
Tracking such statistics is crutial when someone decides whether to buy flat or not.

### Description

For achieving flat statistics relevent data must be supplied. As flat data providers were chosen
- [sreality](http://www.sreality.cz), [bezrealitky](https://www.bezrealitky.cz), [reality](https://www.reality.cz/) and [reality.idnes](https://reality.idnes.cz/). 

[sreality](http://www.sreality.cz) and [bezrealitky](https://www.bezrealitky.cz) providers has available
rest API.

Other approach for retrieving data is to use web scraping. For this purpose there is a pretty web scraping
framework [scrapy](https://docs.scrapy.org/en/latest/) available for python.

This framework let's you define your own crawler with predifined rules, start urls, scraping settings, etc.

The entire solution is scheduled periodically with twisted reactor task scheduler. Every scraped result or result got via rest api is sent to Elasticsearch. 

Kibana web UI presents human readable 
data from Elasticsearch. In Kibana there are graphs with representing flat stats. 

### Rules

<ul>
    <li>There no duplicates data in Elasticsearch.</li>
    <li>There is depth limit for web scraping.</li>
    <li>Web scraping need to load heavy JS webpages therefore headless chrome with selenium is used.</li>
</ul>


### Challenges

> Timestamp of data is challenging. Some providers have records of a flat timestamp.
> However most of providers doesn't track flats timestamp so scheduled job assigns timestamp when
> new record is processed.
 
Technologies:
- [x] **Python**
- [x] **Scrapy framework**
- [x] **Headless Chrome with Selenium**
- [x] ~~Java (Springboot/SpringRest)~~
- [x] **Docker containerization**
- [x] **Elasticsearch**
- [x] **Kibana**(GUI for Elasticsearch data)
- [ ] **Machine learning** 

### Instalation guide

#### Standalone

Download Kibana and Elasticsearch. Run them.
Run python Scheduler.py command (some of packages propably
must be downloaded by pip package manager)

#### Docker way

Docker service must be running!

To build python app use Dockerfile. Run command:

```
docker build -t flatstats-image .
```

Set vm.max_map_count for Elasticsearch.
Recommended value is following

```
sysctl -w vm.max_map_count=262144
```

For persistent effect create file /etc/sysctl.d/60-vm-max_map_count.conf with the content above.


Use docker-compose to setup Elasticsearch, Kibana, Chromeless-Selenium and Flatstats
app docker containers together (run images). Change paths in docker-compose.yml file if necessary.

```
docker-compose up -d
```

[Elasticsearch web access](http://localhost:9200)
[Kibana console web access](http://localhost:5601/app/kibana)


Use docker-compose to shutdown running docker containers.

```
docker-compose down
```

Use docker exec to get into running container's bash

```
sudo docker exec -it flatstats /bin/bash
```
